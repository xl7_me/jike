<?php
/*
*Author:ping
*Email:main@ainixch.cn  
2018年8月13日
　　　　　　　\\\|/// 
　　　　　　\\　.-.-　// 
.　　　　　　(　.@.@　) 
+-------oOOo-----(_)-----oOOo---------+ 
|　　　 　　　　　　　　　　　　　　　| 
|　    　HelloWorld	！					     　| 
|　　　　　　　　　　　　　　　　 　　| 
+---------------------Oooo------------+
Copyright  2018 竹叶科技
*/
//Base控制器
namespace Home\Controller;
use Think\Controller;
class BaseController extends Controller{
    public $user;
    public function _initialize() {
        //配置信息
        $res=M('config')->where(array('group'=>1))->select();
        foreach ($res as $k=> &$v){
            $info[$v['name']]=$v['title'];
           }
            
        $this->assign('config',$info);
        
        //用户信息
        $this->user=session('user');
        if($this->user['user_id']>0){
            $this->assign('user',$this->user);
        }
        else {
            $user['nickname']='游客';
            $user['head_pic']=C('DEF_PIC');
            $user['pay_points']=0;
            $this->assign('user',$user);
        }
        
        //当前访问者地理信息
//         $local=getIpCity();
//         session('city_id',$local['nums']);
//         $this->assign('local',$local);
    }
    
}