<?php
/*
*Author:ping
*Email:main@ainixch.cn  
2018年8月13日
　　　　　　　\\\|/// 
　　　　　　\\　.-.-　// 
.　　　　　　(　.@.@　) 
+-------oOOo-----(_)-----oOOo---------+ 
|　　　 　　　　　　　　　　　　　　　| 
|　    　HelloWorld	！					     　| 
|　　　　　　　　　　　　　　　　 　　| 
+---------------------Oooo------------+
*/
//用户控制器
namespace Home\Controller;
use Home\Model\UserModel as m_user;
use Home\Model\ProductsModel;
class UserController extends BaseController{
    
        public function index(){
            $check_has_mw=M('childstar')->where(array('user_id'=>$this->user['user_id']))->find();
            if(!empty($check_has_mw)){
                $this->assign('child',$check_has_mw);
                $this->assign('has',1);
            }
            $this->display();
        }
        
        //用户登陆
        public function login(){
           if(IS_AJAX){
               $username=I('username');
               $password=I('password');
               $user=new m_user();
               $info=$user->login($username,$password);
               $status=$info['status'];
               $msg=$info['msg'];
               
               //验证失败
               if($info['status']!=2000){
                   $this->ajaxReturn(array('status'=>$status,'msg'=>$msg));
               }
               
               //session保存数据
               session('user',$info['data']);
               $this->ajaxReturn(array('status'=>$status,'msg'=>$msg));
           }

           $this->display();
        }
    
        //第三方登陆
        public function thirdLogin($type = 'weixin'){
            //导入thinkSDK类
            vendor('ThinkSDK.ThinkOauth#class');
            empty($type) && $this->error('参数错误');
             
            $sns = \ThinkOauth::getInstance($type);
            //请求登录
            redirect($sns->getRequestCodeURL());
        }
        
        public function callback($type = null, $code = null){
            (empty($type) || empty($code)) && $this->error('参数错误');
            //加载ThinkOauth类并实例化一个对象
            vendor('ThinkSDK.ThinkOauth#class');
            $sns  = \ThinkOauth::getInstance($type);
        
            //调用方法，实例化SDK对象的时候直接作为构造函数的第二个参数传入
            //如： $qq = ThinkOauth::getInstance('qq', $token);
            $token = $sns->getAccessToken($code , $extend);
            //获取当前登录用户信息
            if(is_array($token)){
                if($type=='weixin'){
                    $wx   = \ThinkOauth::getInstance($type, $token);
                    $data = $wx->call('user/get_user_info');
                    if($data['ret'] == 0){
                        //$this->success('登录成功',U('User/index'));
                        dump($data);
        
                    } else {
                        $this->error("获取用户信息失败：{$data['msg']}");
                    }
                }
            }
        }
        //萌娃显示
        public function showSprout(){
            $id=I('id');
            $res=M('childstar')->where(array('id'=>$id))->find();
            $pro=M('province')->where(array('provinceid'=>$res['pro']))->getField('province');
            $city=M('city')->where(array('cityid'=>$res['city']))->getField('city');
            $area=M('area')->where(array('areaid'=>$res['area']))->getField('area');
            $res['location']=$pro.' '.$city.' '.$area;
            $this->assign('detail',$res);
            $this->display();
        }
    
        //添加萌娃
        public function addBaby(){
            if(IS_POST){
                if($this->user['user_id']<1){
                    $this->error('还没有登陆');
                }
                die;
            }
            $this->display();
        }
    
    
        //设置
        public function setting(){
            $this->display();
        }
        
        //关于
        public function about(){
            $this->display();
        }
        //意见反馈
        public function advice() {
            if(IS_AJAX){
                $cont=I('cont');
                $uid=$this->user['user_id'];
                $add_advi=new ProductsModel();
                $res=$add_advi->addAdvice($cont,$uid);
                $this->ajaxReturn(array('status'=>$res['status'],'msg'=>$res['msg']));
            }
            $this->display();
        }
    
        //我的消息
        public function notice(){
            if(empty($this->user)){
                $this->error('还没有登陆');
            }
            $this->display();
        }
        
        //我的余额
        public function mypocket() {
            if(empty($this->user)){
                $this->error('还没有登陆');
            }
            $this->display();
        }
        
        //通告
        public function Announcement(){
            if(empty($this->user)){
                $this->error('还没有登陆');
            }
            $this->display();
        }
        
        //活动
        public function Activity(){
            if(empty($this->user)){
                $this->error('还没有登陆');
            }
            $this->display();
        }
        
        //收藏
        public function collect(){
            if(empty($this->user)){
                $this->error('还没有登陆');
            }
            $this->display();
        }
        
        //收获地址
        public function address(){
            if(empty($this->user)){
                $this->error('还没有登陆');
            }
            $dress_=new m_user();
            
            $list=$dress_->dressList($this->user['user_id']);
            $this->assign('dress',$list);
            $this->display();
        }
        
        //编辑收获地址
        public function addressEdit(){
            $id=I('id');
            $res=M('dress')->where(array('id'=>$id))->find();
            if(IS_POST){
                $cont=I('post.');
                $id_=$cont['id'];
                unset($cont['id']);
                $cont['user_id']=$this->user['user_id'];
                $res=M('dress')->where(array('id'=>$id_))->save($cont);
                if($res){
                    $this->success('保存成功');
                }
                else $this->error('保存失败');
                die;
            }
            $this->assign('detail',$res);
            $this->display();
        }
        
        //新增收获地址
        public function addressAdd(){
            if(IS_POST){
                $cont=I('post.');
                $cont['user_id']=$this->user['user_id'];
                if($cont['is_def']==1){
                   $def_id=M('dress')->where(array('is_def'=>1))->getField('id');
                  if($def_id>1){
                      M('dress')->where(array('id'=>$def_id))->save(array('is_def'=>0));
                  }
                }
                $res=M('dress')->add($cont);
               if($res){
                    $this->success('保存成功');
                }
                else $this->error('保存失败');
                die;
            }
            $this->display();
        }
        
        //删除收货地址
        public function delDress(){
            $id=I('id');
            $this->ajaxReturn(array('status'=>2000,'msg'=>'删除成功'));
            if($this->user['user_id']>0){
                $res=M('dress')->where(array('id'=>$id))->delete();
                if($res){
                    $this->ajaxReturn(array('status'=>2000,'msg'=>'删除成功'));
                }
                else $this->ajaxReturn(array('status'=>2002,'msg'=>'删除失败'));
            }
        }
        
        //加盟
        public function Join(){
            $this->display();
        }
        
        //机构入驻
        public function Admission_mec(){
           $info=M('org')->where(array('user_id'=>$this->user['user_id']))->find();
     
            if(IS_POST){
                if(empty($this->user)){
                    $this->error('还没有登陆');
                    die;
                }
                $cont=I('post.');
                $pics=explode('|', $cont['imgs']);
                unset($cont['file']);
                unset($cont['imgs']);
                $cont['user_id']=$this->user['user_id'];
                if(!empty($info)){
                   $res=M('org')->where(array('user_id'=>$this->user['user_id']))->save($cont);
                }
                else{
                    $res=M('org')->add($cont);
                }
                $data['goods_id']=$res;
                $data['type']=2;
                $new=M('picture');
                for ($i=0;$i<count($pics);$i++){
                    $pics[$i]=base64ToImg($pics[$i],$this->user['user_id']);
                    $data['path']=$pics[$i];
                    $new->add($data);
                }
                $this->redirect('User/Admission_mec_next');
            }
            $this->display();
        }
        
        public function Admission_mec_next(){
            if(IS_POST){
                $cont=I('post.');
                $data['sfz_z']=base64ToImg($cont['sfz_z'],$this->user['user_id']);
                $data['sfz_f']=base64ToImg($cont['sfz_f'],$this->user['user_id']);
                $data['sfz_sc']=base64ToImg($cont['sfz_sc'],$this->user['user_id']);
                $data['yyzz']=base64ToImg($cont['yyzz'],$this->user['user_id']);
                
                $data['status']=0;
                $res=M('org')->where(array('user_id'=>$this->user['user_id']))->save($data);
               
                if($res){
                    $this->success('添加成功',U('User/index'));
                }
                else $this->error('添加失败');
                die;
            }
            $this->display();
        }
        
        //城市合伙人入驻
        public function Admission_cit(){
            
                if(empty($this->user)){
                    $this->error('还没有登陆');
                    die;
                }
            
            $this->display();
        }
        
        public function Admission_cit_next(){
            $this->display();
        }
        
        
        //vip
        public function vip(){
            $this->display();
        }
        
        
        
        public function upfile(){
            $upload = new \Think\Upload();// 实例化上传类
            $upload->maxSize   =     18145728 ;// 设置附件上传大小
            $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
            //$upload->saveName = '';
            $upload->rootPath  =     './Uploads/'; // 设置附件上传根目录
            $upload->savePath  =     ''; // 设置附件上传（子）目录
            // 上传文件
            $info   =   $upload->upload();
            if(!$info) {
                $this->error($upload->getError());
                exit;
            }else{// 上传成功
                //dump($info);
                foreach($info as $file){
                    $data['datas']= "/Uploads/".$file['savepath'].$file['savename'];
                }
                echo $data['datas'];
            }
             
        
        }
        
        //收支记录
        public function payRecord(){
            $this->display();
        }
        
        
        public function tixian(){
            $this->display();
        }
        
        public function delmw(){
            if(IS_AJAX){
                $res=M('childstar')->where(array('user_id'=>$this->user['user_id']))->delete();
               if($res){
                   $this->ajaxReturn(array('status'=>2000,'msg'=>'删除成功'));
               }
               else  $this->ajaxReturn(array('status'=>2001,'msg'=>'删除失败'));
            }
        }
        
        //添加艺术照 
        public function mw_addimg(){
            $this->display();
        }
        
        
        //添加剧照
        public function mw_add_juzhao(){
            $this->display();
        }
        
        //添加生活照
        public function mw_add_live(){
            $this->display();
        }
        
        
        
        
        
        
}
