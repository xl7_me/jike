<?php
/*
*Author:ping
*Email:main@ainixch.cn  
2018年8月20日
　　　　　　　\\\|/// 
　　　　　　\\　.-.-　// 
.　　　　　　(　.@.@　) 
+-------oOOo-----(_)-----oOOo---------+ 
|　　　 　　　　　　　　　　　　　　　| 
|　    　HelloWorld	！					     　| 
|　　　　　　　　　　　　　　　　 　　| 
+---------------------Oooo------------+
活动控制器
*/
namespace Home\Controller;
use Home\Model\ActivityModel as m_acti;
class ActivityController extends BaseController{
    
    public function index(){
        $city_info=getIpCity();
        $city_num=$city_info['nums'];
        $res=M('activity')->where(Array('location'=>$city_num))->select();
        foreach ($res as &$v){
            if($v['location']<1000){
                $v['location']='全国';
            }
            else {
                $v['location']=M('province')
                ->where(array('provinceid'=>$v['location']))
                ->getField('province');
            }
        }
        $this->assign('acs',$res);
        $this->display();
    }
    
    
    //详情
    public function detail(){
        $id=I('id');
        $res=M('activity')->where(array('id'=>$id))->find();
        $res['location']=M('city')->where(array('cityid'=>$res['location']))->getField('city');
        if(empty($res['location'])){
            $res['location']='全国';
        }
        $imgs=M('picture')->where(array('type'=>3,'goods_id'=>$res['id'],'user_id'=>$this->user['user_id']))->select();
        $check=M('collection')->where(array('goods_id'=>$id,'type'=>2))->find();
        if($check){
            $checks=1;
        }
        else $checks=0;
        
        $check_ac=M('user_activi')->where(array('user_id'=>$this->user['user_id'],'a_id'=>$id))->find();
        if($check_ac){
            $check_as=1;
        }
        else $check_as=0;
        
        if(time()-$res['end']<0){
            $check_as=3;
        }
        //评论
        $coms=M('comment')->where(array('type'=>2,'goods_id'=>$id))->select();
        foreach ($coms as $k=>$v){
            $info[$k]['user_name']=getUserName($v['user_id']);
            $info[$k]['head_pic']=getUserHead($v['user_id']);
            $info[$k]['com']=$v['com'];
            $info[$k]['time']=date("Y.m.d",$v['add_time']);
        }
        $this->assign('nums',count($info));
        $this->assign('com',$info);
        $this->assign('is_c',$checks);
        $this->assign('is_a',$check_as);
        $this->assign('img',$imgs);
        $this->assign('detail',$res);
        $this->display();
    }
    
    
    //更多
    public function more(){
        $this->display();
    }
    
    //列表
    public function qinzi(){
        $list_m=new m_acti();
        $type=I('type')?I('type'):0;
        $this->assign('type',$type);
        $list=$list_m->ActivityList(1);
        $this->assign('list',$list);
        $this->display();
    }
    
    //Ajax返回活动
    public function getActivity(){
        if(IS_AJAX){
            $type=I('type');
//             $page=I('page')?I('page'):1;
            $list_ajax=new m_acti();
            $list=$list_ajax->ActivityList($type);
            if(!empty($list)){
                $this->ajaxReturn(array('status'=>2000,'msg'=>'加载完成','data'=>$list));
            }
            else $this->ajaxReturn(array('status'=>2002,'msg'=>'没有数据了','data'=>''));
        }
        else $this->ajaxReturn(array('status'=>2003,'msg'=>'非法请求'));
    }
    
    
    public function JoinAcity(){
        if(IS_AJAX){
            //             if($this->user['is_aut']!=1){
            //                 $this->ajaxReturn(array('msg'=>'您还没有通过平台认证无法报名参加'));
            //             }
            $id=I('id');
            $uid=$this->user['user_id'];
            $check=M('user_activi')->where(array('user_id'=>$this->user['user_id'],'a_id'=>$id))->find();
            if($check){
                $this->ajaxReturn(array('msg'=>'报过名了'));
            }
            $data['user_id']=$uid;
            $data['a_id']=$id;
            $data['add_time']=time();
            $res=M('user_activi')->add($data);
            M('activity')->where(array('id'=>$id))->setInc('join',1);
            $this->ajaxReturn(array('msg'=>'报名成功'));
    
        }
    } 
    
    
    public function changecoll(){
        if(IS_AJAX){
            $uid=$this->user['user_id'];
            $sta=I('sta');
            $id=I('id');
            if($sta==1){
                $data['goods_id']=$id;
                $data['user_id']=$uid;
                $data['add_time']=time();
                $data['type']=2;
                $res=M('collection')->add($data);
                if($res){
                    $this->ajaxReturn(array('status'=>2000,'msg'=>'添加收藏成功'));
                }
                else $this->ajaxReturn(array('status'=>2001,'msg'=>'添加收藏失败'));
            }
            else {
                $res=M('collection')->where(array('goods_id'=>$id,'user_id'=>$uid,'type'=>2))->delete();
                 
                if($res){
                    $this->ajaxReturn(array('status'=>2000,'msg'=>'删除收藏成功'));
                }
                else $this->ajaxReturn(array('status'=>2000,'msg'=>'删除收藏成功'));
            }
        }
    }
    
    
    
    
    
}