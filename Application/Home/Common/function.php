<?php
/*
 *Author:ping
 *Email:main@ainixch.cn
 2018年8月13日
 　　　　　　　\\\|///
 　　　　　\\　.-.-　//
 　　　　　(　.@.@　)
 　　　　　　　　　　　　　+-------oOOo-----(_)-----oOOo---------+
 　　　　　　　　　　　　　|　　　 　　　　　　　　　　　　　　　|
 　　　　　　　　　　　　　|　    　HelloWorld	！					     　|
 　　　　　　　　　　　　　|　　　　　　　　　　　　　　　　 　　|
 　　　　　　　　　　　　　+---------------------Oooo------------+
 　　　　　　　　　　　　　*/
//Home公共函数库

//获取用户名

function getUserName($id){
    $info=M('user')->where(array('user_id'=>$id))->getField('nickname');
    return $info;
}

//获取用户头像
function getUserHead($id){
    $info=M('user')->where(array('user_id'=>$id))->getField('head_pic');
    return $info;
}

//获取省名字
function getProvince($pid){
    $name=M('province')->where(array('provinceid'=>$pid))->getField('province');
    return $name;
}

//获取城市名字
function getCity($cid){
    $name=M('city')->where(array('cityid'=>$cid))->getField('city');
    return $name;
}


//获取区/县名字
function getArea($aid){
    $name=M('area')->where(array('areaid'=>$aid))->getField('area');
    return $name;
}

//倒计时几天
function getCountDay($time){
    $now=time();
    $_time=$now-$time;
    if($_time>=86400){
        $days=intval($_time/86400);
        return $days."天";
    }
    else {
        $hours=intval($_time/3600);
        return $hours."小时";
    }
}

//获取地区
function getLocation($local){
    if($local<1000){
        return "全国";
    }
    $name=M('city')->where(array('cityid'=>$local))->getField('city');
    return $name;
}


//获取活动的第一张图
function getFirstPic($id){
   $src= M('picture')->where(array('goods_id'=>$id))->getField('path');
   if(empty($src)){
       return C('ACTIVITY_DEF_PIC');
   }
   return $src;
}

//获取访问者IP
function get_real_ip(){
    $ip=false;
    //$ip='59.70.224.55';
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ips=explode (', ', $_SERVER['HTTP_X_FORWARDED_FOR']);
        if($ip){ array_unshift($ips, $ip); $ip=FALSE; }
        for ($i=0; $i < count($ips); $i++){
            if(!eregi ('^(10│172.16│192.168).', $ips[$i])){
                $ip=$ips[$i];
                break;
            }
        }
    }
    return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}

//获取访问者城市
function getIpCity(){
    $ip=get_real_ip();
    if($ip=='127.0.0.1'){
        return array('city'=>'本机保留地址','nums'=>0);
    }
    $ips=explode('.', $ip);
    if($ips[0]==192||$ips[1]==168){
        return array('city'=>'本地局域网','nums'=>implode('.', $ips));
    }
    $url="http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;
    $cont=file_get_contents($url);
    $data=json_decode($cont);
    return array('city'=>$data->data->city,'nums'=>$data->data->city_id);
}



//base64解析图片
function base64ToImg($base64,$user){
    $path="./Uploads/".date('Y-m-d',time());
    //匹配出图片的格式
    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64, $result)){
        $type = $result[2];
        $new_file =$path."/".$user."/";

        if(!file_exists($new_file)){
            //检查是否有该文件夹，如果没有就创建，并给予最高权限
            mkdir($new_file, 0700);
        }
        $new_file = $new_file.time().mt_rand(10, 99).".{$type}";

        if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64)))){
            return $new_file;
        }else{
            return false;
        }
    }else{
        return false;
    }
}





