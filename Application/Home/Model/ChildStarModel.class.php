<?php
/*
*Author:ping
*Email:main@ainixch.cn  
2018年8月21日
　　　　　　　\\\|/// 
　　　　　　\\　.-.-　// 
.　　　　　　(　.@.@　) 
+-------oOOo-----(_)-----oOOo---------+ 
|　　　 　　　　　　　　　　　　　　　| 
|　    　HelloWorld	！					     　| 
|　　　　　　　　　　　　　　　　 　　| 
+---------------------Oooo------------+
童星
*/
namespace Home\Model;
use think\Model;
class ChildStarModel extends Model{
    public function editStar($uid) {
        $res=M('childstar')->where(array('id'=>$uid))->find();
        foreach ($res as &$v){
            $v['tag']=explode('|', $v['tag']);
            foreach ($v['tag'] as &$v2){
                $v2['tag']=M('child_tag')-where(array('id'=>$v2))->getFild('title');
            }
            $v['exp']=M('child_exp')->where(array('user_id'=>$v['id']))->find();
            foreach ($v['exp'] as $key=> $v3){
                $v[$key]=$v3;
            }
            return $res;
        }
    }
    
    
    //童星列表
    public function childList($cat=1) {
         $res=M('childstar')->where(array('cat'=>$cat))->field('head_pic,name,age,height,weight')->select();
         foreach($res as &$v){
             if(empty($v['head_pic'])){
                 $v['head_pic']=C('DEF_PIC');
             }
         }
         return $res;
         
    }
    
    //今日推荐
    public function todayFor(){
        $res=M('childstar')->getField('id',true);
//       return $info;
        for ($i=0;$i<3;$i++){
            $res[$i]=M('childstar')->where(array('id'=>$res[$i]))->find();
        }
        foreach($res as &$v){
            if(empty($v['head_pic'])){
                $v['head_pic']=C('DEF_PIC');
            }
        }
        return $res;
    }
    
}