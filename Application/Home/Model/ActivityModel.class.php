<?php
/*
*Author:ping
*Email:main@ainixch.cn  
2018年8月20日
　　　　　　　\\\|/// 
　　　　　　\\　.-.-　// 
.　　　　　　(　.@.@　) 
+-------oOOo-----(_)-----oOOo---------+ 
|　　　 　　　　　　　　　　　　　　　| 
|　    　HelloWorld	！					     　| 
|　　　　　　　　　　　　　　　　 　　| 
+---------------------Oooo------------+
活动模型
*/
namespace Home\Model;
use Think\Model;
class ActivityModel extends Model{
    //活动列表
    public function ActivityList($type) {
        $res=M('activity')
                        ->where(array('type'=>$type))
                        ->field('title,money,location,end,id')
                        ->select();
        foreach($res as &$v){
            $v['thumb']=getFirstPic($v['thumb']);
            $v['count_day']=getCountDay($v['end']);
            $v['location']=getLocation($local);
        }
        return $res;
    }
    
}