<?php
/*
*Author:ping
*Email:main@ainixch.cn  
2018年8月13日
　　　　　　　\\\|/// 
　　　　　　\\　.-.-　// 
.　　　　　　(　.@.@　) 
+-------oOOo-----(_)-----oOOo---------+ 
|　　　 　　　　　　　　　　　　　　　| 
|　    　HelloWorld	！					     　| 
|　　　　　　　　　　　　　　　　 　　| 
+---------------------Oooo------------+

*/
namespace Home\Model;
use Think\Model;
class UserModel extends Model{
    public function login($username,$password) {
        $user=M('user')->where(array('phone'=>$username))->find();
        if(empty($user)){
            return array('status'=>2001,'msg'=>'没有此用户','data'=>'');
        }
        elseif(md5($password)!=$user['password']){
            return array('status'=>2002,'msg'=>'用户密码错误','data'=>'');
        }
        else return array('status'=>2000,'msg'=>'登陆成功','data'=>$user);
    }
    
    //收货地址
    public function dressList($uid) {
        $list=M('dress')->where(array('user_id'=>$uid))->select();
        foreach ($list as &$v){
            $v['pro']=getProvince($v['pro']);
            $v['city']=getCity($v['city']);
            $v['area']=getArea($v['area']);
        }
        
        return $list;
    }
    
}