<?php
/*
*Author:ping
*Email:main@ainixch.cn  
2018年8月14日
　　　　　　　\\\|/// 
　　　　　　\\　.-.-　// 
.　　　　　　(　.@.@　) 
+-------oOOo-----(_)-----oOOo---------+ 
|　　　 　　　　　　　　　　　　　　　| 
|　    　HelloWorld	！					     　| 
|　　　　　　　　　　　　　　　　 　　| 
+---------------------Oooo------------+
Copyright  2018 竹叶科技
*/
//商品模型
namespace Home\Model;
use Think\Model;
class ProductsModel extends Model{
    
    //列表
    public function allLists(){
        $p=M('products');
        $im=M('picture');
        $list=$p->where(array('on_sale'=>1))->select();
        if(empty($list)){
            return null;
        }

        return $list;
    }
    
    //详情
    public function detail($gid){
        
       $info=M('products')->where(array('goods_id'=>$gid))->find();
       //图片轮播
       $info['imgs']=M('picture')->where(array('goods_id'=>$gid))->field('path')->select();
        
       //是否收藏
       $check=M('collection')->where(array('goods_id'=>$gid))->find();
       
       if($check){
           $info['is_co']=1;
       }
       else 
           $info['is_co']=0;
       
        return $info;
    }
    
    //收藏状态
    public function changeColl($sta,$id,$uid) {
        if($sta==1){
            $data['goods_id']=$id;
            $data['user_id']=$uid;
            $data['add_time']=time();
            $res=M('collection')->add($data);
            if($res){
                return array('status'=>2000,'msg'=>'添加收藏成功');
            }
            else return array('status'=>2001,'msg'=>'添加收藏失败');;
        }
        else {
            $res=M('collection')->where(array('goods_id'=>$id,'user_id'=>$uid))->delete();
            if($res){
                return array('status'=>2000,'msg'=>'删除收藏成功');;
            }
            else return array('status'=>2000,'msg'=>'删除收藏成功');;
        }
    }
    //添加建议
    public function addAdvice($cont,$uid) {
            if($cont==''){
                $ms['status']=2001;
                $ms['msg']='不能有空';
            }
            elseif(strlen($cont)>300){
                $ms['status']=2003;
                $ms['msg']='字数超过限制，不能大于300个字符';
            }
            elseif($uid<0){
                $ms['status']=2002;
                $ms['msg']='没有登陆';
            }
            else{
                $data['user_id']=$uid;
                $data['cont']=$cont;
                $data['add_time']=time();
                $res=M('advice')->add($data);
                if($res){
                    $ms['status']=2000;
                    $ms['msg']='您的建议我们已经收到，工作人员将会用最快的时间查看';
                 }
            }
            return $ms;
    }
    
    //判断购物车是否有
    public function checkCart($id,$uid){
        $res=M('cart')->where(Array('goods'=>$id,'user_id'=>$uid))->find();
        if($res){
            return $res['num'];
        }
        else return 0;
    }
    
    //加入购物车
    public function addCart($gid,$uid){
        $cart=M('cart');
        $nums=$this->checkCart($gid,$uid);
        if($nums>0){
            $res=$cart->where(array('goods_id'=>$gid))->setInc('num',1);
            if($res){
                return true;
            }
            else return false;
        }
        else {
            $data['goods_id']=$gid;
            $data['user_id']=$uid;
            $data['add_time']=time();
            $res=$cart->add($data);
             return true;
           
        }
    }
    
    //移除购物车
    public function delCart($gid,$uid,$num=0){
        $cart=M('cart');
        if($num==0){
            $res=$cart->where(array('goods_id'=>$gid,'user_id'=>$uid))->delete();
            return true;
        }
        else {
            $res=$cart->where(array('goods_id'=>$gid,'user_id'=>$uid))->setDec('num',$num);
            return true;
        }
    }
    
    //读取评论
    public function getComment($gid){
        $coms=M('comment')
                                        ->where(array('goods_id'=>$gid))
                                        ->field('user_id,com,add_time')
                                        ->order('add_time DESC')
                                        ->select();
        foreach ($coms as $k=>$v){
            $info[$k]['user_name']=getUserName($v['user_id']);
            $info[$k]['head_pic']=getUserHead($v['user_id']);
            $info[$k]['com']=$v['com'];
            $info[$k]['time']=date("Y.m.d",$v['add_time']);
        }
        return $info;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}