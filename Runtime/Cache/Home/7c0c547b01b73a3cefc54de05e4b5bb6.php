<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>我的余额 - <?php echo ($config["WEB_SITE_TITLE"]); ?></title>
    <script src="/Public/Home/js/adaptive.js"></script>
    <script src="/Public/Home/js/device.min.js"></script>
    <link rel="stylesheet" href="/Public/Home/css/reset.css">
    <link rel="stylesheet" href="/Public/Home/css/con-header.css">
    <script src="/Public/Home/js/mui.min.js"></script>
    <link href="/Public/Home/css/mui.min.css" rel="stylesheet"/>

    <style>
        h4,h5{margin-top: 0px;margin-bottom: 0px;}

        html{background: #fff;}
        .yue{width: 100%;height: 3rem;background: #00B0B8;padding-top: 0.8rem;}
        .yue p{text-align: center;font-size: 0.66rem;color: #333;font-weight: 600;}

        .content{width: 7.5rem;height: auto;margin: 0 auto;margin-top: 0.88rem;}

     .xiaoxi{}
     .xiaoxi li a{height: 1rem;line-height: 0.8rem;}
     .xiaoxi li img{margin-right: 0.1rem;}
      .jilu{position: relative;font-size: 0.3rem;top: -0.1rem;}

        .tixian{width: 80%;height: 0.9rem;background: #00B0B8;text-align: center;line-height: 0.9rem;color: #ffff;font-size: 0.36rem;
            border-radius: 0.4rem;display: block;position: fixed;bottom: 0.3rem;left: 50%;transform: translate(-50%)}
    </style>
</head>
<body>
<div class="content">
    <div class="top">
        <h4>我的余额</h4>
        <a href="#"><span class="back"></span></a>
    </div>

    <div class="yue">
       <p>0.00</p>
    </div>
    <ul class="mui-table-view xiaoxi">

            <li class="mui-table-view-cell ">
                <a href="<?php echo U('User/payRecord');?>" class="mui-navigate-right">
                    <img src="img/shouzhi.png" alt="">
                    <span class="jilu">收支记录</span>
                </a>
            </li>
    </ul>
    <a href="<?php echo U('User/tixian');?>" class="tixian">申请提现</a>

    <!--<ul class="xiaoxi mui-table-view">-->
        <!--<a href="#">-->
            <!--<li class="mui-table-view-cell">-->
                <!--<img src="img/shouzhi.png" alt="">-->
                <!--<span class="mui-navigate-right">收支记录</span>-->
            <!--</li>-->
        <!--</a>-->

    <!--</ul>-->

</div>

<script src="/Public/Home/js/jquery-1.11.1.js"></script>
<script>
    $(".back").click(function () {
        window.history.back(-1);
    })




</script>
</body>
</html>