<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
    <title>首页 - <?php echo ($config["WEB_SITE_TITLE"]); ?></title>
    <script src="/Public/Home/js/mui.min.js"></script>
    <link href="/Public/Home/css/mui.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/Home/css/swiper-3.4.2.min.css">
    <link rel="stylesheet" href="/Public/Home/css/reset.css">
    <link rel="stylesheet" href="/Public/Home/css/home.css">
    <script src="/Public/Home/js/adaptive.js"></script>
    <script src="/Public/Home/js/device.min.js"></script>
    <link rel="stylesheet" href="/Public/Home/css/btm.css">



    <style>
        .swiper-slide img{width: 100%;}
        .btm li:nth-child(1) a{color: #01B0B8;z-index: 100!important;}
         .home{z-index: 1!important;}
        .mui-content .mui-pull-bottom-pocket  .mui-pull-caption{
            position: fixed;!important;
         bottom: 1.8rem;!important;
            left: 40%;
        }
        html,body{width: 100%;height: 100%;}
        .mui-content .mui-pull-bottom-pocket .mui-pull-loading{
            position: fixed;!important;
            bottom: 1.8rem;!important;
            left: 30%;
        }



    </style>
</head>
<body>


<!--顶部栏-->
<div class="seach">
    <form action="#">
        <img src="/Public/Home/img/h-diwen.png" alt="" class="h-wz">
        <span class="weizhi"><?php echo ($local["city"]); ?></span>
        <input type="search" placeholder="请输入您要搜索的内容" class="sub">
        <img src="/Public/Home/img/sousuo.png" alt="" class="sousuo">
    </form>
</div>


<!--底部栏-->
    <ul class="btm">
        <li>
            <a href="<?php echo U('Index/Index');?>">
                首页
                <img src="/Public/Home/img/ft1.2.png" alt="">
            </a>
        </li>
        <li>
            <a href="<?php echo U('Index/childStar');?>">
                童星
                <img src="/Public/Home/img/ft2.1.png" alt="">
            </a>
        </li>

        <li>
            <a href="<?php echo U('Index/shop');?>">
                商城
                <img src="/Public/Home/img/ft3.1.png" alt="">
            </a>
        </li>

        <li>
            <a href="<?php echo U('User/Index');?>">
                个人
                <img src="/Public/Home/img/ft4.1.png" alt="">
            </a>
        </li>


    </ul>

  <div class="home  pullrefresh mui-scroll-wrapper mui-content">
      <div class="mui-scroll cont">
              <!--轮播区域-->
              <div class="swiper-container">
                  <div class="swiper-wrapper">
                      <div class="swiper-slide"><img src="/Public/Home/img/banner1.png" alt="">
                      </div>
                      <div class="swiper-slide"><img src="/Public/Home/img/banner1.png" alt="">
                      </div>
                      <div class="swiper-slide"><img src="/Public/Home/img/banner1.png" alt="">
                      </div>

                  </div>
                  <!-- 如果需要分页器 -->
                  <div class="swiper-pagination"></div>
              </div>


              <ul class="nav">
                 <li>  <a href="<?php echo U('Index/Announcement');?>">通告<img src="/Public/Home/img/h-2.1.png" alt=""></a></li>
                  <li><a href="<?php echo U('Activity/index');?>">活动<img src="/Public/Home/img/h2.2.png" alt=""></a></li>
                  <li><a href="<?php echo U('Index/childStar');?>">童星<img src="/Public/Home/img/h-2.3.png" alt=""></a></li>
                  <li><a href="<?php echo U('Index/nearby');?>">附近<img src="/Public/Home/img/h-2.4.png" alt=""></a></li>
                  <li><a href="<?php echo U('Activity/more');?>">更多<img src="/Public/Home/img/h-2.5.png" alt=""></a></li>
              </ul>

             <!--极客头条-->
              <div class="toutiao">
                  <a href="h-xinwen.html">
                     <img src="/Public/Home/img/头@2x.png" alt="">
                  </a>
                  <div class="notice">
                      <div class="swiper-containers swiper-notice">
                          <div class="swiper-wrapper">
                              <div class="swiper-slide">双十二购物狂欢节正式拉开序幕双十二购物狂欢节正式拉开序幕</div>
                              <div class="swiper-slide">全场三折起</div>
                              <div class="swiper-slide">最高满三百减50</div>
                              <div class="swiper-slide">阿迪达斯专卖双十二回馈顾客</div>

                          </div>
                          <!-- Add Pagination -->
                          <!--<div class="swiper-pagination"></div>-->
                      </div>
                  </div>
              </div>

          <!--中间横幅-->
          <a href="<?php echo U('User/Join');?>" class="ruzhu"><img src="/Public/Home/img/ruzhu.png" alt=""></a>



              <!--热卖通告-->
                 <div class="remai">
                    <a href="#">
                     <h4>热门通告
                         <img src="/Public/Home/img/fanhui2.png" alt="">
                     </h4>
                    </a> 
                 </div>

          <!-- 左右滑动 -->
          <div class="slide-lr">
              <div class=" swiper_second2">
                  <div class="swiper-wrapper">
                      <!--swiper-slide 一项-->
                    <?php if(is_array($ac)): $i = 0; $__LIST__ = $ac;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="swiper-slide">
                          <a href="<?php echo U('Activity/detail',array('id'=>$vo[id]));?>"><img src="<?php echo ($vo["pic"]); ?>"></a>
                          <p class="danhang"><?php echo ($vo["title"]); ?></p>
                          <nav> <span >已报名 <b><?php echo ($vo["join"]); ?></b>人 </span><span ><?php echo ($vo["location"]); ?></span><span >剩余<?php echo ($vo["count_day"]); ?></span></nav>

                      </div><?php endforeach; endif; else: echo "" ;endif; ?> 
                  </div>
                  <!-- Add Pagination -->
                  <!--  <div class="swiper-pagination seconds-pagination"></div> -->
              </div>
          </div>




          <!--最新活动-->
          <div class="zuixin">
            <a href="<?php echo U('Activity/index');?>">
              <h4>最新活动
                  <img src="/Public/Home/img/fanhui2.png" alt="">
              </h4>
            </a>  
          </div>

       <ul class="zx-list">
          <?php if(is_array($new)): $i = 0; $__LIST__ = $new;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
                 <img src="<?php echo ($vo["pic"]); ?>" alt="" class="xin-img" style="height: 120px">
                 <p class="duohang"><?php echo ($vo["title"]); ?></p>
                 <span class="zx-price"><b>￥<?php echo ($vo["money"]); ?></b>起</span>
                 <img src="/Public/Home/img/xp-bg.png" alt="" class="xp-bg">
                 <span class="jiezhi"><?php echo ($vo["count_day"]); ?>后截止</span>
                 <span class="didian"><?php echo ($vo["location"]); ?></span>

             </li><?php endforeach; endif; else: echo "" ;endif; ?>   
       </ul>

          <!--推荐童星-->
          <div class="tuijian">
            <a href="<?php echo U('Index/childStar');?>">
              <h4>推荐童星
                  <img src="/Public/Home/img/fanhui2.png" alt="">
              </h4>
             </a> 
          </div>
          <ul class="tj-list">
            <?php if(is_array($childs)): $i = 0; $__LIST__ = $childs;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo U('Index/childStar');?>">
                  <img src="<?php echo ($vo["head_pic"]); ?>" alt="" class="tj-show">
                 <p><?php echo ($vo["name"]); ?><img src="/Public/Home/img/h-tubiao1.png" alt=""><img src="/Public/Home/img/h-tubiao2.png" alt=""></p>
                  <nav><span><?php echo ($vo["age"]); ?>岁</span> <span><?php echo ($vo["height"]); ?>cm</span> <span><?php echo ($vo["weight"]); ?>kg</span></nav>
                  </a>
              </li><?php endforeach; endif; else: echo "" ;endif; ?>  
          </ul>


              <div style="clear:both"></div>

              <div class="more">

              </div>



      </div>

  </div>

  <script src="/Public/Home/js/jquery-1.11.1.js"></script>
  <script src="/Public/Home/js/swiper-3.4.2.min.js"></script>
<script>
    $(".sub").focus(function () {
        window.location.href="<?php echo U('Index/search');?>"
    })
</script>
  <script>

      var swiper = new Swiper('.swiper-container', {
          autoplay:3000,
          loop:true,
          pagination: '.swiper-pagination',

      });

      var swipers = new Swiper('.swiper-notice', {
          direction: 'vertical',
          autoplay : 3000,
          loop : true,

      });

      var swiper2 = new Swiper('.swiper_second2', {
          slidesPerView: 1.5,

          centeredSlides: false,
          // 如果需要分页器


      });

  </script>


<script>
    mui('body').on('tap', 'a', function () { document.location.href = this.href; });
    // window.addEventListener('touchmove', fn, { passive: false });
    mui.init({
        pullRefresh : {
            container:".pullrefresh",//下拉刷新容器标识，querySelector能定位的css选择器均可，比如：id、.class等
            down : {
                height:50,//可选,默认50.触发下拉刷新拖动距离,
                auto: false,//可选,默认false.自动下拉刷新一次

                contentdown : "下拉可以刷新",//可选，在下拉可刷新状态时，下拉刷新控件上显示的标题内容
                contentover : "释放立即刷新",//可选，在释放可刷新状态时，下拉刷新控件上显示的标题内容
                contentrefresh : "正在刷新...",//可选，正在刷新状态时，下拉刷新控件上显示的标题内容
                callback :pulldownRefresh,//必选，刷新函数，根据具体业务来编写，比如通过ajax从服务器获取新数据；
            },
            up : {
                height : 50,
                auto : false,
                contentrefresh : "正在加载...",
                contentnomore : '没有更多数据了',
                contentup:"上拉加载更多",
                callback : pullupRefresh
            }
        }
    });
    //下拉刷新
    function pulldownRefresh() {
        setTimeout(function() { //三秒后执行函数
            window.location.reload();//刷新
            mui('.pullrefresh').pullRefresh().endPulldownToRefresh(); //用来停止刷新
        }, 1000);
    }
    //上拉加载
    function pullupRefresh() {
        var self = this;
        setTimeout(function() {
            var ul = self.element.querySelector('.more');
            var html = ''
            jQuery(ul).append(html);

        }, 1000);


        setTimeout(function() { //3秒后执行函数
            //停止刷新（加载）

            mui('.pullrefresh').pullRefresh().endPullupToRefresh(false);
            // mui('.pullrefresh').pullRefresh().endPullupToRefresh(true);//true加载的那个会变成，没有更多数据了
        }, 1000);
    }

    // mui.ajax("请求url", {
    //     type: "get",
    //     dataType: "json",
    //     data:{
    //         param:param,
    //         name:name
    //     },
    //     success: function(data){
    //         //将获取到的数据动态赋值给列表，假设scroller为滚动容器
    //         //如果数据不到一页，显示“没有更多数据了”,关闭上拉功能
    //         //scroller.endPullupToRefresh(true);
    //         //scroller.querySelector(".mui-pull-bottom-pocket .mui-pull-caption").innerHTML = "没有更多数据了";
    //         //如果对于关闭的上拉功能，可以通过以下语句重置上拉加载功能。
    //         //mui('#pullrefresh').pullRefresh().refresh(true);
    //     }
    // });





</script>
</body>
</html>