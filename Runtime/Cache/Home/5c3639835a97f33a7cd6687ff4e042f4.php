<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>商城 - <?php echo ($config["WEB_SITE_TITLE"]); ?></title>

    <link href="/Public/Home/css/mui.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/Home/css/swiper-3.4.2.min.css">
    <link rel="stylesheet" href="/Public/Home/css/reset.css">
    <link rel="stylesheet" href="/Public/Home/css/btm.css">
    <script src="/Public/Home/js/adaptive.js"></script>
    <script src="/Public/Home/js/device.min.js"></script>
    <script src="/Public/Home/js/mui.min.js"></script>

    <style>

        .btm li:nth-child(3) a{color: #00B0B8}
        body{background: #fff;}


        .home{width: 7.5rem;height: auto;margin: 0 auto ;background: #ffff;padding-bottom: 2rem;}

        .top{width: 7.5rem;height: 2.3rem;background: #3FBDC2;padding: 0.5rem 0.3rem;position: relative}
        .top .tx{width: 1.3rem;height: 1.3rem;border-radius: 50%;margin-right: 0.2rem;}
        .top .user{position: absolute;top: 0.8rem;color: #fff;font-size: 0.34rem;}
        .top .jifen{ position: absolute;width: 1.2rem;height: 0.4rem;text-align: center;line-height: 0.4rem;display: block;left: 1.7rem;
            bottom: 0.5rem;font-size: 0.24rem;
            border-radius: 0.5rem;color: #fff;     background: -webkit-linear-gradient(left,#015DB8, #01ADB8);}

        .top .wdjf{position: absolute;top: 0.8rem;right: 0.3rem;color: #fff;text-align: center;font-size: 0.24rem;}
        .top .wdjf b{font-size: 0.6rem;color: #fff;font-weight: 700}

        /*分类*/
        .fenlei{width: 100%;height: 0.72rem;display: flex;justify-content: space-around;line-height: 0.72rem;padding: 0.1rem 0rem}
.fenlei span{width: 30%;text-align: center;border-right: 1px solid #EEEEEE}
.fenlei span:nth-child(3){border-right: none}
        /*下拉列表*/
        .cover {
            display: none;
            position: fixed;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.3);
            z-index: 100000;
        }

        .rows {
            width: 100% !important;
            height: 4rem;
            overflow: scroll;
            background: white;
            border-top: 1px solid #E4E4E4;
        }

        .rows p {
            font-size: 0.32rem;
            width: 90%;
            margin: 0 auto;
            color: #666666;
            border-bottom: 1px solid #E4E4E4;
            line-height: 1rem;
        }

        .rows p:last-child {
            border: none;
        }
        .screen2_color{color: #3FBDC2}


         /*我可兑换*/
         .tle{text-align: center;color: #333;font-size: 0.36rem;font-weight: 700;margin-top: 0.4rem;border-bottom: 1px solid #DEDEDE;
             line-height: 0.5rem;height: 0.7rem;}
        .list1{width: 100%;height: auto;display: flex;justify-content: space-around}
        .list1 li{text-align: center;width: 33%}
        .list1 li .jf{color: #FDAA91;font-size: 0.32rem}
        .list1 li img{width: 2rem;height: 2rem;}


        .list2{width: 100%;height: auto;overflow: hidden}
        .list2 li{text-align: center;float: left;width: 33.3%;background: #fff}
        .list2 li .jf{color: #FDAA91;font-size: 0.32rem}
        .list2 li img{width: 2rem;height: 2rem;}
    </style>
</head>
<body>
<div class="home">
    <!--底部栏-->
    <ul class="btm">
        <li>
            <a href="<?php echo U('Index/index');?>">
                首页
                <img src="/Public/Home/img/ft1.1.png" alt="">
            </a>
        </li>
        <li>
            <a href="<?php echo U('Index/childStar');?>">
                童星
                <img src="/Public/Home/img/ft2.1.png" alt="">
            </a>
        </li>

        <li>
            <a href="<?php echo U('Index/shop');?>">
                商城
                <img src="/Public/Home/img/ft3.2.png" alt="">
            </a>
        </li>

        <li>
            <a href="<?php echo U('User/Index');?>">
                个人
                <img src="/Public/Home/img/ft4.1.png" alt="">
            </a>
        </li>


    </ul>




    <div class="top">
            <img src="<?php echo ($user["head_pic"]); ?>" alt="" class="tx">
            <span class="user"><?php echo ($user["nickname"]); ?></span>
             <a href="sc-duihuan.html" class="jifen">积分明细</a>
            <span class="wdjf"><b><?php echo ($user["pay_points"]); ?></b><br>我的积分</span>
    </div>

    <div class="fenlei">
         <span>类型 <img src="/Public/Home/img/dbx.png" alt=""></span>
         <span>销量 <img src="/Public/Home/img/dbx.png" alt=""></span>
         <span>价格 <img src="/Public/Home/img/dbx.png" alt=""></span>
    </div>
    <!--下拉列表-->
    <div class="cover">
        <div class="rows">
            <p class="rows_lists">红色</p>
            <p class="rows_lists">蓝色</p>
            <p class="rows_lists">红色</p>
            <p class="rows_lists">蓝色</p>
        </div>
    </div>


    <!--我可兑换-->
     <p class="tle">我可兑换</p>
    <ul class="list1">
        <?php if(is_array($can)): $i = 0; $__LIST__ = $can;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
                <a href="<?php echo U('Index/pro_detail',array('id'=>$vo[goods_id]));?>">
                <img src="<?php echo ($vo["pro_pic"]); ?>" alt="">   <p class="danhang"><?php echo ($vo["name"]); ?></p>
                <p class="jf"><?php echo ($vo["point"]); ?>积分</p>
                </a>
            </li><?php endforeach; endif; else: echo "" ;endif; ?>


    </ul>

    <!--全部商品-->

    <p class="tle">全部商品</p>
    <ul class="list2">
        <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
                <a href="<?php echo U('Index/pro_detail',array('id'=>$vo[goods_id]));?>"><img src="<?php echo ($vo["pro_pic"]); ?>" alt="">   <p class="danhang"><?php echo ($vo["name"]); ?></p>
                <p class="jf"><?php echo ($vo["point"]); ?>积分</p>
                </a>
            </li><?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>

</div>
<script src="/Public/Home/js/jquery-1.11.1.js"></script>
<script>

    $(".fenlei span").click(function() {
        $(this).addClass("screen2_color").siblings().removeClass("screen2_color");
        $(".cover").animate({
            opacity: 1
        }, 200, function() {
            $(this).show()
        });
    })
    $(".cover").click(function() {
        $(".cover").animate({
            opacity: 0
        }, 200, function() {
            $(this).hide()
        });
    });
</script>
</body>
</html>