<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>收货地址 - <?php echo ($config["WEB_SITE_TITLE"]); ?></title>
    <script src="/Public/Home/js/adaptive.js"></script>
    <script src="/Public/Home/js/device.min.js"></script>
    <link rel="stylesheet" href="/Public/Home/css/reset.css">
    <link rel="stylesheet" href="/Public/Home/css/con-header.css">
    <script src="/Public/Home/js/mui.min.js"></script>
    <link href="/Public/Home/css/mui.min.css" rel="stylesheet"/>

    <style>
        h4,h5{margin-top: 0px;margin-bottom: 0px;}
        input::-webkit-input-placeholder,
        textarea::-webkit-input-placeholder {
            color: #9B9B9B;font-size: 0.3rem;
        }
        html{background: #F0F0F0}
        .content{width: 7.5rem;height: auto;margin: 0 auto;background: #F0F0F0;margin-top: 0.88rem;}
        .top a{position: absolute;right: 0.3rem;bottom: 0.1rem;color: #fff;}
     .dizhi{width: 100%;height: 2.3rem;margin-top: 0.2rem;background: #fff;padding: 0.24rem 0.3rem;position: relative}
        .dizhi span{font-size: 0.3rem;color: #666}
        .dizhi span:nth-child(2){float: right}
        .dizhi p{margin-top: 0.15rem;width: 100%;position: relative}
        .dizhi p:after{content: "";width: 100%;height: 1px;background: #DEDEDE;position: absolute;bottom:-0.2rem;left: 0px;}
        .dizhi  .bianji{float: right;margin-top: 0.1rem;margin-right: 0.4rem;}
        .dizhi  .bianji a{color: #8C8C8C}
        .dizhi  .bianji img{width: 0.32rem;height: 0.32rem;margin-right: 0.15rem}
        .dizhi  .shanchu{float: right;margin-top: 0.1rem}
        .dizhi  .shanchu a{color: #8C8C8C}
        .dizhi  .shanchu img{width: 0.32rem;height: 0.32rem;margin-right: 0.15rem }




        .dizhi  .danxuan{width: 2rem;height: 0.7rem;display: inline-block;line-height:0.7rem;color: #666}
        .dizhi  .danxuan input[type=checkbox]{width: 0.4rem;height: 0.4rem;display:inline-block;display: none}

        .advice{height: 0.4rem;width: 0.4rem;display: inline-block;margin-right: 0.2rem;
            background-image: url("/Public/Home/img/p-shdz.png");
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            vertical-align: middle;
            margin-top: -4px;
           border-radius: 0.05rem;
        }
        input[type="checkbox"]:checked + .advice{
            height: 0.4rem;width: 0.4rem;
            background-image: url("/Public/Home/img/p-shdz2.png");
            border-radius: 0.1rem;
        }
    </style>
</head>
<body>
<div class="content">
    <div class="top">
        <h4>我的收货地址</h4>
        <span class="back"></span>
        <a href="<?php echo U('User/addressAdd');?>">新增</a>
    </div>
        <?php if(is_array($dress)): $i = 0; $__LIST__ = $dress;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="dizhi">
               <span><?php echo ($vo["accept"]); ?></span>
               <span><?php echo ($vo["tel"]); ?></span>
                 <p><?php echo ($vo["pro"]); echo ($vo["city"]); echo ($vo["area"]); echo ($vo["detail"]); ?></p>
                 <span class="danxuan">
                   <input type="checkbox" name="type" id="adviceRadio1" checked="none" value="0"  hidden/>
                    <label for="adviceRadio1" class="advice"></label>
                     默认地址</span>

                 <span class="shanchu" onclick="delDress(<?php echo ($vo["id"]); ?>)"><img src="/Public/Home/images/shanhcu2.png" alt="#"><a>删除</a></span>
                 <span class="bianji"><img src="/Public/Home/images/bianiji.png" alt="#"><a href="<?php echo U('User/addressEdit',array('id'=>$vo[id]));?>">编辑</a></span>
            </div><?php endforeach; endif; else: echo "" ;endif; ?>
    
</div>
<script src="/Public/Home/js/jquery-1.11.1.js"></script>
</div>
<script src="/Public/Home/layer_m/layer.js"></script>
<script>
    $(".back").click(function () {
        window.history.back(-1);
    })

    function delDress(id){
        $.ajax({
                method:'post',
                url:"<?php echo U('User/delDress');?>",
                dataType:'json',
                data:{
                    id:id
                },
                success:function(data){
                      layer.open({
                            content: data.msg
                            ,skin: 'msg'
                            ,time: 2 //2秒后自动关闭
                          });
                      if(data.status==2000){
                        $(".shanchu").parent().remove();
                      }
                }
        })
    }





</script>
</body>
</html>