<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <script src="/Public/Home/js/adaptive.js"></script>
    <script src="/Public/Home/js/device.min.js"></script>
    <link rel="stylesheet" href="/Public/Home/css/reset.css">
    <link rel="stylesheet" href="/Public/Home/css/con-header.css">
    <script src="/Public/Home/js/mui.min.js"></script>
    <link href="/Public/Home/css/mui.min.css" rel="stylesheet"/>

    <style>
        h4,h5{margin-top: 0px;margin-bottom: 0px;}
        html{background: #fff;}
        .content{width: 7.5rem;height: auto;margin: 0 auto;margin-top: 0.88rem;background: #fff}
        .bnner{width: 100%;height: 0.8rem;border-bottom: 1px solid #DEDEDE;display: flex;justify-content: space-around;line-height: 0.8rem;color: #333}

        .h-show{color: #19B7BE}
        .hd{width: 100%;height: auto;position: relative}
        .hd ul{width: 100%;height: auto;position: absolute;left: 0px;top: 0px;padding:0.4rem  0.3rem;display: none}
        .hd .hd-list{display: block}
        /*1*/
        .hd-list li{width: 100%;height: 2.4rem;border-bottom: 1px solid #DEDEDE;position: relative;margin-bottom: 0.4rem;}
        .hd-list li .xin-img{width: 2rem;height: 2.4rem;}
        .hd-list li p{position: absolute;left: 2.2rem;top: 0rem;height: 0.8rem;line-height: 0.4rem;color: #000;font-size: 0.3rem;}
        .hd-list li .zx-price{position: absolute;left: 2.2rem;top: 1.2rem;height: 0.8rem;;color: #999;font-size: 0.26rem;}
        .hd-list li .zx-price b{font-size: 0.32rem;}
        .hd-list li .xp-bg{position: absolute;left: 2.2rem;bottom: 0.1rem;color: #000;font-size: 0.26rem;}
        .hd-list li .jiezhi{position: absolute;left: 2.3rem;bottom: 0.2rem;color: #fff;font-size: 0.24rem;}
        .hd-list li .didian{position: absolute;top:50%;transform: translateY(-50%);right: 0.3rem;color: #333}
        .hd-list li .tuikuan{position: absolute;width: 1.2rem;height: 0.5rem;text-align: center;line-height: 0.5rem;display: block;
            border: 1px solid #999;right: 0px;bottom: 0.2rem;font-size: 0.24rem;border-radius: 0.5rem;color: #666}
        .hd-list li .queren{position: absolute;width: 1.2rem;height: 0.5rem;text-align: center;line-height: 0.5rem;display: block;
            right: 0px;bottom: 0.2rem;font-size: 0.24rem;border-radius: 0.5rem;color: #fff;background:  -webkit-linear-gradient(left,#015DB8, #01ADB8)}

        /*2*/
        .hd-list li .p1{position: absolute;left: 2.2rem;top: 1.8rem;height: 0.8rem;;color: #999;font-size: 0.26rem;}
        .hd-list li .canyu{position: absolute;left: 2.2rem;top: 1.1rem;height: 0.8rem;;color: #999;font-size: 0.26rem;}
        .hd-list li .p1 b{font-size: 0.32rem;}


        /*3*/
        .hd-list li .suc{position: absolute;bottom:0.2rem;right: 0.3rem;color: #FC6132}
    </style>
</head>
<body>
<div class="content">
    <div class="top">
        <h4>我的活动</h4>
         <span class="back"></span>
    </div>

    <ul class="bnner">
        <li class="h-show">待参与</li>
        <li>已完成</li>
        <li>退款订单</li>
    </ul>

    <div class="hd">
       <ul class="hd-list">
           <li>
               <img src="/Public/Home/img/zuixin.png" alt="" class="xin-img">
               <p class="duohang">香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中</p>
               <span class="zx-price"><b>￥3699.00 </b>起</span>
               <img src="/Public/Home/img/xp-bg.png" alt="" class="xp-bg">
               <span class="jiezhi">15天后截止</span>
               <span class="didian">全国</span>
                <span class="tuikuan">申请退款</span>

           </li>
           <li>
               <img src="/Public/Home/img/zuixin.png" alt="" class="xin-img">
               <p class="duohang">香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中</p>
               <span class="zx-price"><b>￥3699.00 </b>起</span>
               <img src="/Public/Home/img/xp-bg.png" alt="" class="xp-bg">
               <span class="jiezhi">15天后截止</span>
               <span class="didian">全国</span>
               <span class="tuikuan">申请退款</span>

           </li>
       </ul>
        <ul>
            <li>
                <img src="/Public/Home/img/zuixin.png" alt="" class="xin-img">
                <p class="duohang">香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中</p>
                <span class=" p1"><b>￥3699.00 </b>起</span>
              <span class="canyu">正在参与..</span>
                <span class="didian">全国</span>
                <span class="queren">确认完成</span>
            </li>
            <li>
                <img src="/Public/Home/img/zuixin.png" alt="" class="xin-img">
                <p class="duohang">香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中</p>
                <span class=" p1"><b>￥3699.00 </b>起</span>
                <span class="canyu">正在参与..</span>
                <span class="didian">全国</span>
                <span class="queren">确认完成</span>
            </li>
        </ul>
        <ul>
            <li>
                <img src="/Public/Home/img/zuixin.png" alt="" class="xin-img">
                <p class="duohang">香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中</p>
                <span class=" p1"><b>￥3699.00 </b>起</span>
                <span class="canyu">全国</span>
                <span class="suc">成功</span>
            </li>
            <li>
                <img src="/Public/Home/img/zuixin.png" alt="" class="xin-img">
                <p class="duohang">香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中香港拓展活动7天6夜正在火爆招募中</p>
                <span class=" p1"><b>￥3699.00 </b>起</span>
                <span class="canyu">全国</span>
                <span class="suc">成功</span>
            </li>
        </ul>
    </div>





</div>

<script src="/Public/Home/js/jquery-1.11.1.js"></script>
<script>
    $(".back").click(function () {
        window.history.back(-1);
    })
$(".bnner li").click(function () {
    $(this).addClass("h-show").siblings().removeClass();
    let num = $(this).index();
    $(".hd ul").eq(num).addClass("hd-list").siblings().removeClass();
})



</script>
</body>
</html>