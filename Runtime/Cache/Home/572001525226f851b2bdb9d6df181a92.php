<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>加盟 - <?php echo ($config["WEB_SITE_TITLE"]); ?></title>
    <script src="/Public/Home/js/adaptive.js"></script>
    <script src="/Public/Home/js/device.min.js"></script>
    <link rel="stylesheet" href="/Public/Home/css/reset.css">
    <link rel="stylesheet" href="/Public/Home/css/con-header.css">
    <script src="/Public/Home/js/mui.min.js"></script>
    <link href="/Public/Home/css/mui.min.css" rel="stylesheet"/>

    <style>
        h4,h5{margin-top: 0px;margin-bottom: 0px;}
        input::-webkit-input-placeholder,
        textarea::-webkit-input-placeholder {
            color: #9B9B9B;font-size: 0.3rem;
        }
        html{background: #fff}
        .content{width: 7.5rem;height: auto;margin: 0 auto;background: #fff;margin-top: 0.88rem;}

           .mui-content{background: #fff;}
           .mui-content ul {width: 100%;padding-top: 0.6rem;}
           .mui-content ul li {width: 100%;text-align: center;position: relative;margin-bottom: 0.4rem}
        .mui-content ul li .s5{width: 6rem;height: 2.72rem;}
        .mui-content ul li .s6{width: 6rem;height: 1rem;}
           .mui-content ul li a {width: 3rem;height:0.8rem;text-align: center;color: #fff;border-radius: 0.5rem;line-height: 0.8rem;background: #47D4BB;
           display: inline-block;bottom: 0.4rem;}

        .mui-content ul li  .rz2{background: #EBC247}
        .mui-content ul li b{font-size: 0.48rem;font-weight: 600;color: #333;left: 1.2rem}

    </style>
</head>
<body>
<div class="content">
    <div class="top">
        <h4>我要加盟</h4>
        <span class="back"></span>
    </div>

    <div class="mui-content">
         <ul>
             <li>
                 <img src="/Public/Home/img/ruzhu1.png" alt="" class="s5" >
                 <a href="<?php echo U('User/Admission_mec');?>" class="spjz">立即入驻</a>
             </li>
             <li>
                 <img src="/Public/Home/img/ruzhu2.png" alt="" class="s5">
                 <a href="<?php echo U('User/Admission_cit');?>" class="spjz rz2">立即入驻</a>
             </li>
             <li>
                 <img src="/Public/Home/img/dh1.png" alt="" class="s6">
                   <b class="tr"><?php echo ($config["WEB_SITE_PHONE"]); ?></b>
             </li>
         </ul>
    </div>


</div>
<script src="/Public/Home/js/jquery-1.11.1.js"></script>
<script>
    $(".back").click(function () {
        window.history.back(-1);
    })

    $(".shanchu").click(function () {
        $(this).parent().remove();
    })





</script>
</body>
</html>